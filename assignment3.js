
$(document).ready(function() {
$("#fname").blur(function (){
	var fnameval=document.getElementById("fname").value;
    if($(this).val().length == 0){
        //$(this).after('<div style=" color:red"> Firstname is Required</div>');
        $("#ferr").css("color","red").text("Firstname is Required");
    }
     else if(fnameval.match(/^[a-zA-Z]+$/))
      {
      	//document.getElementById("demo").innerHTML="<font color='green'>Valid</font>";
      	// $(this).after('<div style=" color:green"> Valid</div>');
      	$("#ferr").css("color","green").text("Valid");
      }
    else
      {
          //document.getElementById("demo").innerHTML="<font color='red'>Please enter your firstname only in text</font>";
          //$(this).after('<div style=" color:red">Please enter your firstname only in text</div>');
          $("#ferr").css("color","red").text("Please enter your firstname only in text");  
      }

});
});

$(document).ready(function() {
$("#lname").blur(function (){
	var lnameval=document.getElementById("lname").value;
    if($(this).val().length == 0){
        //$(this).after('<div style="color:red">Lastname is Required</div>');
        $("#lerr").css("color","red").text("Lastname is Required");
    }
    else if(lnameval.match(/^[a-zA-Z]+$/))
      {
      	//document.getElementById("demo").innerHTML="<font color='green'>Valid</font>";
      	//$(this).after('<div style=" color:green"> Valid</div>');
      	$("#lerr").css("color","green").text("Valid");
      }
    else
      {
          //document.getElementById("demo").innerHTML="<font color='red'>Please enter your firstname only in text</font>";
          //$(this).after('<div style=" color:red">Please enter your lastname only in text</div>');
          $("#lerr").css("color","red").text("Please enter your lastname only in text");
          
      }
});
});

$(document).ready(function() {
$("#about").blur(function(){
    if($(this).val().length == 0){
        //$(this).after('<div style="color:red">About you is Required</div>');
        $("#abouterr").css("color","red").text("About you is Required");
    }

    else
            {
              //document.getElementById("aboutMsg").innerHTML="<font color='green'>Valid</font>";
                //$(this).after('<div style="color:green">Valid</div>');
                 $("#abouterr").css("color","green").text("Valid");
            }
});
});
$(document).ready(function() {
$("#about").on("keypress", function(e) {
if (e.which === 32 && !this.value.length)
e.preventDefault();
});
});

$(document).ready(function() {
$("#phone").blur(function(){
	var phn=document.getElementById("phone").value;
	var phnlen=Number(phn.length);

    if($(this).val().length == 0){
        //$(this).after('<div style="color:red">Phone number is Required</div>');
        $("#phoneerr").css("color","red").text("Phone Number is Required");
    }
    else if (phn[0] == '+' &&( phnlen == 13 || phnlen == 12)) {
        //document.getElementById("message").innerHTML="<font color='green'>Valid</font>";
        //$(this).after('<div style="color:green">Valid</div>');
        $("#phoneerr").css("color","green").text("Valid");

   } else if (phn[0] == '+' && phnlen !== 13) {
            //document.getElementById("message").innerHTML="<font color='red'>invalid</font>";
             //$(this).after('<div style="color:red">Invalid</div>');
             $("#phoneerr").css("color","red").text("Invalid");
   } else if (phn[0] !== '+') {
      if (phnlen > 9 && phnlen < 13) {
                    //document.getElementById("message").innerHTML="<font color='green'>valid</font>";
                    //$(this).after('<div style="color:green">Valid</div>');
                    $("#phoneerr").css("color","green").text("Valid");

      } else {
                    //document.getElementById("message").innerHTML="<font color='red'>invalid</font>";
                     //$(this).after('<div style="color:red">Invalid</div>');
                     $("#phoneerr").css("color","red").text("Invalid");


      }
   } 

});
});

$(document).ready(function() {
$("#office").blur(function(){
   var ofycNum=document.getElementById("office").value;
   var numbers = /^[0-9]+$/;
   if(ofycNum.match(numbers))
      {
      
        //document.getElementById("ofyc").innerHTML="<font color='green'>Valid</font>";
        //$(this).after('<div style="color:green">Valid</div>');
         $("#ofycerr").css("color","green").text("Valid");
    
      }
      else
      {
        //document.getElementById("ofyc").innerHTML="<font color='red'> Enter only numbers</font>";
        //$(this).after('<div style="color:red">Enter only numbers</div>');
         $("#ofycerr").css("color","red").text("Enter only numbers");
      }
});
});


$(document).ready(function() {
$("#email").blur(function(){
var mail=document.getElementById("email").value;
var maillen=Number(mail.length);
var mailformat= /^\S\D\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}\.[a-zA-Z]{2}$/;
var mailformat1= /^\S\D\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
var mailformat2= /^\D\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
  if(mail.match(mailformat2) && mail.match(mailformat1)|| mail.match(mailformat))
    {
      //document.getElementById("print").innerHTML="<font color='green'>Valid</font>";
      $("#emailerr").css("color","green").text("Valid");

    }  
    else
    {
      //document.getElementById("print").innerHTML="<font color='red'> Enter valid email-address</font>";
    	$("#emailerr").css("color","red").text("Enter valid email-address");
    }

    
});
});

$(document).ready(function() {
$("#password").blur(function(){
	var pwdGen=document.getElementById("password").value;
      var pwdlen=Number(pwdGen.length);
    var checkPwd=/(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,12})$/;
    
    if($(this).val().length == 0){
	        //$(this).after('<div style="color:red">Password is Required</div>');
	         $("#passworderr").css("color","red").text("Password is Required");
	    }
      else if(pwdGen.match(checkPwd))
     {
      //document.getElementById("pwdMsg").innerHTML="<font color='green'>Valid</font>";
      //$(this).after('<div style="color:green">Valid</div>');
       $("#passworderr").css("color","green").text("Valid");
     }
     else{
     	 $("#passworderr").css("color","red").text("8-12 range no special characters");
     	 //$(this).after('<div style="color:red">It should be alphanumberic and should be 8-12 range no special characters</div>');
        //document.getElementById("pwdMsg").innerHTML="<font color='red'>It should be alphanumberic and should be 8-12 range no special characters</font>";
     }
});
});

$(document).ready(function() {
$("#confirmpassword").blur(function(){
	var confirmPassword=document.getElementById("confirmpassword").value;
	var password = document.getElementById("password").value;
    //var pwdlen=Number(pwdGen.length);
    //var checkPwd=/(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,12})$/;
    
    if($(this).val().length == 0){
	        //$(this).after('<div style="color:red">Confirm Password is Required</div>');
	         $("#confirmerr").css("color","red").text(" Confirm Password is Required");
	    }
   
    else if (password != confirmPassword) {
    	 $("#confirmerr").css("color","red").text("Passwords do not match");
    	//$(this).after('<div style="color:red">Passwords do not match</div>');
           //document.getElementById("confirmMsg").innerHTML="<font color='red'>Passwords do not match</font>";
       }
    else{
          //document.getElementById("confirmMsg").innerHTML="<font color='green'>Valid</font>";
          //$(this).after('<div style="color:green">Valid</div>');
           $("#confirmerr").css("color","green").text("Valid");

        }
});
});


$(document).ready(function() {
$("#age,#month,#year,#day").change(function (){
    var date=new Date;
    var birthMonth=document.getElementById('month').value;
    //onsole.log(birthMonth);

    var birthYear=document.getElementById('year').value;
    //console.log(birthYear);
    var birthDay=document.getElementById('day').value;
    //console.log(birthDay);
     if((birthMonth=='Month') || (birthDay=='Day') || (birthYear=='Year'))
	    {
	      document.getElementById("age").value= " ";
	      return false;
	    }
	  else{  
		    var dob=new Date(birthYear,birthMonth/10,birthDay);
		    var m=date.getMonth()+1;
		    var y=date.getFullYear()-1;
		    var q=birthMonth-m;
		    var age=(y-birthYear)+(Math.abs(12-q)/10)
		    document.getElementById("age").value=age;
		}    
    document.getElementById("age").disabled = true;

});
});


$(document).ready(function() {
$(".radio").click(function(){
	var rad=document.getElementsByClassName("radio");
    var okay=false;
        for(var i=0,l=rad.length;i<l;i++)
          {
             if(rad[i].checked)
             {
                  okay=true;
                  break;
              }
           }
          if(okay)
            //document.getElementById("demogen").innerHTML="<font color='green'>Gender selected.!</font>";
        	//$(this).after('<div style="color:green">Gender selected!</div>');
        	 $("#gendererr").css("color","green").text("Gender selected!!");


          else 
              //document.getElementById("demogen").innerHTML="<font color='red'>Select atleast one radio button</font>";
          	//$(this).after('<div style="color:red">Select atleast one radio button</div>');
          	 $("#gendererr").css("color","red").text("Select atleast one radio button");

});
});

$(document).ready(function() {
$(".checkbox").click(function(){
	var checkd=document.getElementsByClassName("checkbox");
    var okay=false;
          for(var i=0,l=checkd.length;i<l;i++)
            {
              if(checkd[i].checked)
                {
                  okay=true;
                  break;
                }
            }
          if(okay)
          	$("#interesterr").css("color","green").text("Interest  selected!!");
          	//$(this).after('<div style="color:green">Thank you for checking a checkbox</div>');
            //document.getElementById("checkMsg").innerHTML="<font color='green'>Thank you for checking a checkbox</font>";

          else 
          	$("#interesterr").css("color","red").text("Select atleast one checkbox");
          	 //$(this).after('<div style="color:red">Select atleast one checkbox</div>');
              //document.getElementById("checkMsg").innerHTML="<font color='red'>Select atleast one checkbox</font>";
});
});

function validation(){

   if( dob2()==false || check()==false)
    {
        return false;
    }
     else
    {
       return true;
    }	
}

function dob2(){

var Month=document.getElementById('month').value;
var Year=document.getElementById('year').value;
var Day=document.getElementById('day').value;
if((Month==('Month'))||(Year==('Year'))||(Day==('Day'))){
	$("#dobMsg").css("color","red").text("Please Select DOB");
    //document.getElementById("dobMsg").innerHTML = "<font color='red'> Please select DOB</font>";
        return false;
      }
else{

//document.getElementById("dobMsg").innerHTML = "<font color='green'>DOB selected</font";
$("#dobMsg").css("color","green").text("DOB seleted");
}


}
// function check(){
// 	var fnameval=$("#fname").val();
//     var fnamelen=Number(fnameval.length);
//      if(fnamelen===0)
//       {
//         $("#ferr").css("color","red").text("Firstname is Required");
     
//       }

//     else if(fnameval.match(/^[a-zA-Z]+$/))
//       {
//       	$("#ferr").css("color","green").text("Valid");	
//       }
//     else
//       {
//       		 $("#ferr").css("color","red").text("Please enter your firstname only in text");
//           //document.getElementById("demo").innerHTML="<font color='red'>Please enter your firstname only in text</font>";
//            return false;
//       }
// }

// function mand(){
// 	var lnameval=document.getElementById("lname").value;
//     var lnamelen=Number(lnameval.length);
//     	if(lnamelen===0)
//       	{
//        //document.getElementById("demo").innerHTML="this is invalid name";
//        		 $("#lerr").css("color","red").text("Firstname is Required");
//         // document.getElementById("fname").value="";
//         // document.name_fileds.fname.focus();
//       	}

//        else if(lnameval.match(/^[a-zA-Z]+$/))
//         {
//           $("#lerr").css("color","green").text("Valid");
//         }
//   	else
//       {
//       	 $("#ferr").css("color","red").text("Please enter your lastname only in text");
//    			return false;
//       }

// }
//  function phoneCheck() 
  		
//   {
//    var phn=document.getElementById("phone").value; 
//    var phnlen=Number(phn.length);
//    var gb = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
//    var reg=/^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$/;
//    var validReg=/^\+?[0-9-]+$/;
//    var countryReg=/^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$/;
//    var newReg=/^([+][9][1]|[9][1]|[0]){0,1}([7-9]{1})([0-9]{9})$/;
//    if(phnlen === 0)
//      {
//         //document.getElementById("message").innerHTML="<font color='red'>Mandatory Field</font>";
//          $("#phoneerr").css("color","red").text("Phone Number is Required");
//     }
//         return false;
//      }
//    //console.log(phn);
//     //var pattern = new RegExp("([^\d])\d{10}([^\d])");

//    // if (pattern.test(document.getElementById('phone').value))
  
//    //else if(phn.match(/^\d{10}$/))
//    if (phn[0] == '+' &&( phnlen == 13 || phnlen == 12)) {
//    			 $("#phoneerr").css("color","green").text("Valid");

//         //document.getElementById("message").innerHTML="<font color='green'>Valid</font>";
//    } else if (phn[0] == '+' && phnlen !== 13) {
//              $("#phoneerr").css("color","red").text("Invalid");

//             return false;
//    } else if (phn[0] !== '+') {
//       if (phnlen > 9 && phnlen < 13) {
//       	 $("#phoneerr").css("color","green").text("Valid");

//                     //document.getElementById("message").innerHTML="<font color='green'>valid</font>";
//       } else {
//       	             $("#phoneerr").css("color","red").text("Invalid");

//                     //document.getElementById("message").innerHTML="<font color='red'>invalid</font>";
//                     return false;

//       }
//    } 
//    function checkOffice(){
//   var ofycNum=document.getElementById("office").value;
//    var numbers = /^[0-9]+$/;
//    if(ofycNum.match(numbers))
//       {
      
//         $("#ofycerr").css("color","green").text("Valid");
    
//       }
//       else
//       {
//       $("#ofycerr").css("color","red").text("Invalid");
//         return false;
//       }


//   }
//   function checkMail() {
//        var mail=document.getElementById("email").value;
//        var maillen=Number(mail.length);
//        var mailformat= /^\S\D\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}\.[a-zA-Z]{2}$/;
//         var mailformat1= /^\S\D\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
//         var mailformat2= /^\D\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
//       if(mail.match(mailformat2) && mail.match(mailformat1)|| mail.match(mailformat))
//       {
//           $("#emailerr").css("color","green").text("Valid");
//       }  
//       else
//         {
//              $("#emailerr").css("color","red").text("Inalid");
//             return false;
//        }
      
//   }
// function checkPassword(){
//       var pwdGen=document.getElementById("pwd").value;
//       var pwdlen=Number(pwdGen.length);
//     var checkPwd=/(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,12})$/;
//     if(pwdlen===0)
//     {
//      $("#passworderr").css("color","red").text("Password is Required");
//     }
//       else if(pwdGen.match(checkPwd))
//      {
//        $("#passworderr").css("color","green").text("Vaid");
//      }
//      else{
//        $("#passworderr").css("color","red").text("8-12 range no special characters");
       
//         return false;
//      }
//    }
//     function confirmPassword() {
//      // body...
//         var password = document.getElementById("pwd").value;
//         var passwordlen=Number(password.length);
//         var confirmPassword = document.getElementById("confirm").value;
//         if(passwordlen===0)
//         {
//           $("#confirmerr").css("color","red").text(" Confirm Password is Required");
//         }
//          else if (password != confirmPassword) {
//            $("#confirmerr").css("color","red").text("Passwords do not match");
//            return false;
//         }
//         else{
          
//         	$("#confirmerr").css("color","green").text("Valid");
//         }
        
//    }
//    function about()
// {
//     var details=document.getElementById("aboutYou").value;
//     var detailslen=Number(details.length); 
//      var nospace=/^\s*\s*$/;  
//         if(detailslen===0)
//             {
//             	 $("#abouterr").css("color","red").text("About you is Required");
//               //document.getElementById("aboutMsg").innerHTML="<font color='red'>Mandatory Field</font>";
//             }
//           else if(details.match(nospace)) 
//         {
//         	$("#abouterr").css("color","green").text("Valid");
//             //document.getElementById("aboutMsg").innerHTML="<font color='red'>Should not be empty</font>";
//             return false;
//         }      

//         else
//             {
//             	$("#abouterr").css("color","green").text("Valid");

//               //document.getElementById("aboutMsg").innerHTML="<font color='green'>Valid</font>";
//             }
  
// }