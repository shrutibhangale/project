

var taskInput=document.getElementById("new-task");
var addButton=document.getElementsByTagName("button")[0];
var incompleteHolder=document.getElementById("incomplete-tasks");
var completeHolder=document.getElementById("completed-tasks");



var createNewTaskElement=function(taskString){

  var listItem=document.createElement("li");


  var checkBox=document.createElement("input");
  var label=document.createElement("label");
  var editInput=document.createElement("input");
  var editButton=document.createElement("button");

  var deleteButton=document.createElement("button");

  label.innerText=taskString;

  checkBox.type="checkbox";
  editInput.type="text";
  editButton.innerText="Modify_Task";
  editButton.style.display="flex";
  //deleteButton.style.backgroundColor = 'red';
  editButton.style.justifyContent="space-around";
  editButton.style.marginLeft="237px";

  editButton.className="edit";

  deleteButton.innerText="Delete_Task";
  deleteButton.style.display="flex";
  deleteButton.style.justifyContent="space-around";
  deleteButton.style.marginLeft="45px";

  deleteButton.className="delete";

  listItem.appendChild(checkBox);
  listItem.appendChild(label);
  listItem.appendChild(editInput);
  listItem.appendChild(editButton);
  listItem.appendChild(deleteButton);
  return listItem;
}



function load(){
  var completeHolder=document.getElementById("completed-tasks");
    //window.localStorage.clear();
    var getTasks = window.localStorage.getItem('arrayList');
    if(getTasks == null || getTasks == undefined || getTasks === '') {
      window.localStorage.setItem('arrayList', JSON.stringify([]));
    }
    var fetchTasks = JSON.parse(window.localStorage.getItem('arrayList'));
    console.log('onload task list is: ', fetchTasks);
    for(var i=0;i<fetchTasks.length;i++)
    {
              var fetching=fetchTasks[i].task;
              console.log(fetching);
              // debugger 
              if(fetchTasks[i].status==="complete") {
              var listItem=createNewTaskElement(fetching);
              completeHolder.appendChild(listItem);
              triggerEvents(listItem, taskCompleted);
            }
            else
            {
               var listItem=createNewTaskElement(fetching);
             incompleteHolder.appendChild(listItem);
              triggerEvents(listItem, taskCompleted);
              
            }
    }

  }

var addTask=function(){
     if(taskInput.value.match(/^\s*\s*$/) ) 
          {
             alert("Enter something valid..!");
             false;
          }
      else{
              //alert("Add task?");
              console.log("Add Task...");
              var fetchTasks = JSON.parse(window.localStorage.getItem('arrayList'));
              fetchTasks.push({task: taskInput.value, status: 'incomplete'})
              window.localStorage.setItem('arrayList', JSON.stringify(fetchTasks));
              let getTask = JSON.parse(window.localStorage.getItem('arrayList'));
              //debugger;
              var listItem=createNewTaskElement(taskInput.value);


             incompleteHolder.appendChild(listItem);
              triggerEvents(listItem, taskCompleted);
              taskInput.value=" ";
          }
}


var editTask=function(){
      var listItem=this.parentNode;
      var editInput=listItem.querySelector('input[type=text]');
      var label=listItem.querySelector("label");
      var containsClass=listItem.classList.contains("editMode");
      var value = listItem.childNodes[1].textContent;
      if(containsClass){
        label.innerText=editInput.value;
      }else{
        editInput.value=label.innerText;
      }
      listItem.classList.toggle("editMode");
      var fetchTasks =JSON.parse(window.localStorage.getItem('arrayList'));
      let index = fetchTasks.findIndex(taskInfo => taskInfo.task === value)
      fetchTasks[index].task = editInput.value;
      window.localStorage.setItem('arrayList', JSON.stringify(fetchTasks));
}


var deleteTask=function(){
        console.log("Delete Task...");

        var listItem=this.parentNode;
        var ul=listItem.parentNode;
        
        ul.removeChild(listItem);
        var fetchTasks = JSON.parse(window.localStorage.getItem('arrayList'));
        let value = listItem.innerText.split('\n')[0];
        var index=fetchTasks.findIndex(fetch => fetch.task === value);
        fetchTasks.splice(index,1);
        window.localStorage.setItem('arrayList', JSON.stringify(fetchTasks));


}


var taskCompleted=function(){
        console.log("Complete Task...");
        var listItem=this.parentNode;
        completeHolder.appendChild(listItem);
        triggerEvents(listItem, taskIncomplete);
        var fetchTasks = JSON.parse(window.localStorage.getItem('arrayList'));
        let value = listItem.childNodes[1].textContent;
        console.log(value);
        //debugger;
        // fetchTasks.push({task: value, status: 'complete'})
        // window.localStorage.setItem('arrayList', JSON.stringify(fetchTasks));
        // let getTask = JSON.parse(window.localStorage.getItem('arrayList'));
         //debugger
        fetchTasks.forEach((item) =>{
          console.log(item.task)
          if(item.task === value){
            console.log('in if')
             item.status="complete";
             console.log(item)
           }
        })
        //arr.findIndex(img => img.name ==="abc");

            //finding index to get the particular lists
              //var index=fetchTasks.findIndex(fetch => fetch.task === value);
            //console.log(index);
            // //changing the status of the task
                // fetchTasks[index].status="complete";
                  window.localStorage.setItem('arrayList', JSON.stringify(fetchTasks));
            //console.log(fetchTasks);
       //triggerEvents(listItem, taskCompleted);

}


var taskIncomplete=function(){
    console.log("Incomplete Task...");
    var listItem=this.parentNode;
    incompleteHolder.appendChild(listItem);
      //triggerEvents(listItem,taskCompleted);
       triggerEvents(listItem, taskIncomplete);
       
}



var doIt=function(){
}

addButton.onclick=addTask;
//addButton.addEventListener("click",addTask);
addButton.addEventListener("click",doIt);


var triggerEvents=function(taskListItem,checkBoxEventHandler){
 

  var checkBox=taskListItem.querySelector("input[type=checkbox]");
  var editButton=taskListItem.querySelector("button.edit");
  var deleteButton=taskListItem.querySelector("button.delete");
      editButton.onclick=editTask;
     
      deleteButton.onclick=deleteTask;
      //checkBox.onclick=checkBoxEventHandler;
      checkBox.onchange=checkBoxEventHandler;
}


  // for (var i=1; i<incompleteTaskHolder.children.length() -1 ;i++){

  //   triggerEvents(incompleteTaskHolder.children[i],taskCompleted);
  // }
  for (var i=0; i<incompleteHolder.children.length ;i++){

    triggerEvents(incompleteHolder.children[i],taskCompleted);
  }



  // for (var i=1; i<completeHolder.children.length() -1 ;i++){
  
  //   triggerEvents(completeHolder.children[i],taskIncomplete);
  // }
  for (var i=0; i<completeHolder.children.length ;i++){
  
    triggerEvents(completeHolder.children[i],taskIncomplete);
  }
//   function check()
// {
//      var taskval=document.getElementById("new-task").value;
//         console.log(taskval)
       
//      if(taskval.match(/^\s*\s*$/) ) 
//       {
//          alert("Enter something valid..!");
//          false;
//       }
//       else
//       {
//         addButton.onclick=addTask;
//       }
    
// }